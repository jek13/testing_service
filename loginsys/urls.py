from django.conf.urls import include, url
from loginsys import views

urlpatterns = [
    url(r'^logout/', views.logout),
    url(r'^', views.login),
]

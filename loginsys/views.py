# -*- coding: utf-8 -*-
from django.shortcuts import render_to_response, redirect
from django.contrib import auth

from testing_service.settings import DEFAULT_URL_FOR_MANAGER, DEFAULT_URL_FOR_TESTABLE


def login(request):
    args = {}

    user = auth.get_user(request)
    if not user.is_anonymous:
        return redirect(DEFAULT_URL_FOR_MANAGER if user.profile.is_manager() else DEFAULT_URL_FOR_TESTABLE)

    if request.method != 'POST':
        return render_to_response('login.html', args)
    else:
        username = request.POST.get('inputLogin', '')
        password = request.POST.get('inputPassword', '')
        user = auth.authenticate(username=username, password=password)
        if user is None:
            args['auth_error'] = 'Не удалось авторизовать пользователя с введенным сочетанием логина и пароля!'
            return render_to_response('login.html', args)
        else:
            auth.login(request, user)
            return redirect(DEFAULT_URL_FOR_MANAGER if user.profile.is_manager() else DEFAULT_URL_FOR_TESTABLE)


def logout(request):
    auth.logout(request)
    return redirect('/')

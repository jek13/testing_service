import datetime

from django.contrib import auth
from django.contrib.auth.models import User
from django.http import HttpResponseNotFound, HttpResponse
from django.shortcuts import render_to_response, redirect

from test_module.models import Test, TestPassing, Question, Answer, ResultOnQuestion, CalcResult, TeacherTest
from test_module.util import get_manager_test_filter, culc_result
from user_profile.models import Profile, Institution
from users_relations.models import AllowedTypes, UsersRelations, RelationType


def tests(request):
    user = auth.get_user(request)
    if user.is_anonymous:
        return redirect('/')

    is_teacher = user.profile.is_teacher()

    # Словарь с учениками для учетеля для которых он должен пройти тестирование
    users = {}
    teacher_uncompleted_tests = []
    if is_teacher:
        teacher_tests_id = list(
            map(lambda tti: tti.test_id, AllowedTypes.objects.filter(user_type_id=user.profile.user_type_id)))
        teacher_users_id = list(map(lambda tid: tid.user_id, UsersRelations.objects.filter(user_second_id=user.pk)))

        # Формируем список, какие тесты нужно удалить из пройденных тестов, если они там есть
        # Если в таблице TeacherTest отсутствует запись с учеником
        # или в PassingTest последний тест не заверешен - добавляем тест в этот список
        for teacher_test_id in teacher_tests_id:
            for teacher_user_id in teacher_users_id:
                if not TeacherTest.objects.filter(passing_test__test_id=teacher_test_id,
                                                  student_id=teacher_user_id).exists() or not TestPassing.objects.get(
                    pk=TeacherTest.objects.filter(passing_test__test_id=teacher_test_id,
                                                  student_id=teacher_user_id).last().passing_test_id).test_complete:
                    teacher_uncompleted_tests.append(teacher_test_id)

        # Формуриуем список из пользователей учителя, где ключ - профиль пользователя,
        # значение - прошел ли учитель тест за ученика
        for teacher_user in teacher_users_id:
            profile = Profile.objects.get(user_id=teacher_user)
            is_exists = TeacherTest.objects.filter(
                student_id=teacher_user).exists()
            if is_exists:
                is_complete = TestPassing.objects.get(
                    pk=TeacherTest.objects.filter(student_id=teacher_user).last().passing_test_id).test_complete
            else:
                is_complete = True
            is_complete_and_exists = is_exists and is_complete
            users[profile] = is_complete_and_exists

    # Получаем id тестов доступных пользователю для прохождения
    allowed_types = AllowedTypes.objects.filter(user_type_id=user.profile.user_type_id)
    allowed_types_id = list(map(lambda ati: ati.test_id, allowed_types))

    # Отбираем список пройденных тестов пользователя
    passing_tests = TestPassing.objects.filter(user=user).exclude(test_complete=False)

    # Удаляем для учителя тесты, которые он прошел для всех учеников
    if is_teacher:
        passing_tests = passing_tests.exclude(test_id__in=teacher_uncompleted_tests)

    # Получаем список идентификаторов
    passing_tests_ids = list(map(lambda pt: pt.test.pk, passing_tests))

    # Получаем список активных тестов (не пройденных)
    a_tests = Test.objects.filter(is_active=True, pk__in=allowed_types_id).exclude(
        pk__in=passing_tests_ids)

    # Создаём словарь: ключ - тест, значение - пройден до конца или нет. Если тест проходится в первые, значение True
    active_tests = {}
    for a_test in a_tests:
        if TestPassing.objects.filter(test_id=a_test.pk, user_id=user.pk).exists():
            active_tests[a_test] = TestPassing.objects.filter(test_id=a_test.pk, user_id=user.pk).last().test_complete
        else:
            active_tests[a_test] = True

    # Тоже самое для пройденных тестов. Если пользователь проходил тест заного и не прошел его до конца
    p_tests = Test.objects.filter(pk__in=passing_tests_ids)
    passing_tests = {}
    for p_test in p_tests:
        passing_tests[p_test] = TestPassing.objects.filter(test_id=p_test.pk, user_id=user.pk).last().test_complete

    return render_to_response('tests.html',
                              {'active_tests': active_tests,
                               'passed_tests': passing_tests,
                               'username': user.profile.name_with_abbreviation(),
                               'is_teacher': is_teacher,
                               'users': users})


def manager_tests(request):
    user = auth.get_user(request)
    if user.is_anonymous or not user.profile.is_manager():
        return redirect('/')

    allowed_types = AllowedTypes.objects.filter(user_type_id=1)
    allowed_types_id = list(map(lambda allowed_types_id: allowed_types_id.test_id, allowed_types))

    active_tests = Test.objects.filter(is_active=True, pk__in=allowed_types_id).all()

    test_dict = {}
    for test in active_tests:
        p_test = TestPassing.objects.filter(test=test).exclude(test_complete=False)
        test_dict[test] = 0 if not p_test else len(p_test)

    return render_to_response('manager_tests.html',
                              {'tests': test_dict,
                               'username': user.profile.name_with_abbreviation(),
                               'is_manager': True})


def management(request):
    user = auth.get_user(request)
    if user.is_anonymous or not user.profile.is_manager():
        return redirect('/')

    institutions = Institution.objects.all()

    return render_to_response('management.html',
                              # тут список процессов импорта
                              {'username': user.profile.name_with_abbreviation(),
                               'institutions': institutions,
                               'is_manager': True})


def passed(request, t_id=1):
    user = auth.get_user(request)
    if user.is_anonymous or not user.profile.is_manager():
        return redirect('/')

    return render_to_response('manager_test.html', {
        'tests_passing_school_class_filter': TestPassing.objects.filter(test_id=t_id)
                              .values_list('user__profile__school_class', flat=True).distinct().order_by(),
        'schools': Institution.objects.all(),
        'tests_passing_id': t_id,
        'username': user.profile.name_with_abbreviation(),
        'is_manager': True
    })


def rez_info(request, t_id=1):
    user = auth.get_user(request)
    if user.is_anonymous or not user.profile.is_manager():
        return redirect('/')

    user_id = TestPassing.objects.get(pk=t_id).user_id
    test_passing = TestPassing.objects.filter(user_id=user_id, pk__lte=t_id).order_by('date_pass').reverse()[:5]
    test_passing_id = list(map(lambda p_test: p_test.pk, test_passing))

    return render_to_response('manager_test_rez_info.html', {
        'test_passing': TestPassing.objects.filter(pk=t_id).get(),
        'username': user.profile.name_with_abbreviation(),
        'is_manager': True,
        'p_test_len': len(test_passing_id)})


def ajax_passed_filter(request):
    user = auth.get_user(request)
    if user.is_anonymous or not user.profile.is_manager():
        return render_to_response('elements\manager_test_filter.html', {
            'test_filtering': ''
        })

    return render_to_response('elements\manager_test_filter.html', {
        'test_filtering': TestPassing.objects.filter(**(get_manager_test_filter(request))).exclude(test_complete=False)
    })


def test_for_manager(test):
    return {'test': test, 'count': TestPassing.objects.get(test=test).count()}


def testable(request, t_id=1):
    user = auth.get_user(request)
    student_id = request.GET.get('user_id')

    q_id = 1
    if TestPassing.objects.filter(test_id=t_id, user_id=user.pk).exists():
        last_p_test = TestPassing.objects.filter(test_id=t_id, user_id=user.pk).last()
        is_test_complete = last_p_test.test_complete
    else:
        is_test_complete = True

    if not is_test_complete:
        q_id = ResultOnQuestion.objects.filter(test=last_p_test).count() + 1
    if not Question.objects.filter(test_id=t_id).exists():
        return HttpResponseNotFound("<h2>В тесте нет ни одного вопроса</h2>")
    return render_to_response('test.html',
                              {'test_id': t_id, 'initial_q_id': q_id, 'is_test_complete': is_test_complete,
                               'student_id': student_id})


# Получение следующего вопроса
def next_question(request):
    questions = Question.objects.filter(test_id=request.GET.get('test_id'))
    question = questions.get(number=request.GET.get('q_id'))

    count = questions.count()
    q_id = request.GET.get('q_id')

    params = {
        'question': question,
        # Ответы на вопрос
        'answers': Answer.objects.filter(answer_question_id=question.question_desc.pk),
        # Несколько ли ответов в текущем вопросе
        'manyanswers': question.question_desc.answer_number,
        # Отправляем в шаблон номер текущего вопроса, чтобы иметь возможность узнать номер следущего
        'q_id': q_id,
        # Отправляем в шаблон общее количество вопросов в тесте
        'questions_number': count,
        'progress': int(int(q_id) / count * 100)
    }

    return render_to_response('elements/test_question.html', params)


# Конечная страница теста, если пользователь до шел до сюда - значит тест пройден полностью
def test_complete(request, t_id=1):
    user = auth.get_user(request)
    p_test = TestPassing.objects.filter(test_id=t_id, user_id=user.pk).last()
    p_test.test_complete = True
    p_test.save()
    return render_to_response('test_complete.html')


# Сохранение данных в бд
def create(request):
    if request.is_ajax():

        test_id = request.POST.get('test_id')
        q_id = request.POST.get('q_id')
        answers = request.POST.getlist('answers[]')
        is_test_complete = request.POST.get('test_complete')
        student_id = request.POST.get('student_id')
        user = auth.get_user(request)
        time = datetime.datetime.now()

        again_pass = True if TestPassing.objects.filter(test_id=test_id, user_id=user.pk).exists() else False
        test_passing = TestPassing.objects.filter(test_id=test_id, user_id=user.pk).last()
        if q_id and answers:
            for number in answers:
                question = Question.objects.get(test_id=test_id, number=q_id).question_desc
                answer = Answer.objects.get(number=number, answer_question_id=question.pk)
                ResultOnQuestion.objects.get_or_create(test=test_passing, question=question, answer=answer.answer,
                                                       ball=answer.answer_weight)

        elif test_id and not q_id and is_test_complete == 'true':
            TestPassing.objects.get_or_create(test=Test.objects.get(id=test_id), user=user, date_pass=time,
                                              again_pass=again_pass)

            if student_id:
                TeacherTest.objects.get_or_create(passing_test=test_passing, student_id=student_id, teacher=user)

    return HttpResponse(status=204)


# Получение ответов на вопросы
def ajax_answers(request):
    user = auth.get_user(request)
    if user.is_anonymous or not user.profile.is_manager():
        return render_to_response('elements\manager_answers', {
            'answers': ''
        })

    result_on_question = ResultOnQuestion.objects.filter(test__pk=request.GET.get('p_test_id'))
    # Создаём словарь: ключ - вопрос, значение - ответ
    answers = {}
    for result in result_on_question:
        answer = answers.get(result.question.question_desc)
        # Если такой ключ уже существует, значит вопрос с множественным ответом,
        # тогда к значению ключа добавляем текущий вопрос
        if answer:
            answer = {
                result.question.question_desc: answer + ', {} ({})'.format(result.answer.description, result.ball)}
            answers.update(answer)
        else:
            answers[result.question.question_desc] = '{} ({})'.format(result.answer.description, result.ball)

    return render_to_response('elements\manager_answers.html', {
        'answers': answers
    })


# Получение результатов на вопросы
def ajax_result(request):
    user = auth.get_user(request)
    if user.is_anonymous or not user.profile.is_manager():
        return render_to_response('elements\manager_results.html', {
            'results': ''
        })

    p_test_id = request.GET.get('p_test_id')
    test_id = TestPassing.objects.get(pk=p_test_id)
    formulas = CalcResult.objects.filter(test_id=test_id.test_id)

    labels = {}
    for label in formulas:
        labels[label.label] = '{} ({})'.format(
            culc_result(label.formula, test_id.test_id, p_test_id, test_id.user, label), label.annotation)

    return render_to_response('elements\manager_results.html', {
        'results': labels
    })


# Получение ответов родителя/учителя
def ajax_relation_answer(request):
    p_test_id = request.GET.get('p_test_id')
    user_id = TestPassing.objects.get(pk=p_test_id).user_id
    if request.GET.get('parent'):
        relation_type = RelationType.objects.get(relation='Родитель-Ребенок')
        user_parents = list(map(lambda us: us.user_second.user,
                                UsersRelations.objects.filter(user_id=user_id, relation_type=relation_type)))
        users_p_test_ids = list(map(lambda user: TestPassing.objects.filter(user=user).last().pk, user_parents))
        return get_parents_answer(request, users_p_test_ids)
    else:
        relation_type = RelationType.objects.get(relation='Учитель-ученик')
        user_p_test_id = TeacherTest.objects.filter(teacher_id=UsersRelations.objects.get(
            user_id=user_id, relation_type=relation_type.pk).user_second_id, student_id=user_id).last().passing_test_id
    request.GET = request.GET.copy()
    request.GET['p_test_id'] = user_p_test_id

    return ajax_answers(request)


# Функция получения списка вопросов родителя и учителя для отображения в таблице
def get_parents_answer(request, p_test_ids):
    result_on_question = ResultOnQuestion.objects.filter(test_id__in=p_test_ids).order_by(
        'question__question__number')

    # Создаём лист из QuerySet result_on_question чтобы можно было изменять значения и они сохранились при отправке
    answers = list(result_on_question)
    # Берём первый элемент как "предыдущий", чтобы не было ошибки на первой итерации цикла
    prev_answer = answers[0]

    for answer in answers[1:]:
        # Если номер текущего вопроса и номер предыдущего вопроса совпадает, значит имеем дело с вопросом
        # с множественными ответами и в этом случае добавляем к description ответа значение description предыдущего
        # ответа и удаляем его(чтобы не отображался)

        if answer.question_id == prev_answer.question_id and answer.test_id == prev_answer.test_id:
            answer.answer.description = '{} ({}), {}'.format(prev_answer.answer.description, prev_answer.ball,
                                                             answer.answer.description)
            answers.remove(prev_answer)

        # Сохраняем текущее значение вопроса для следующей итерации цикла
        prev_answer = answer

    return render_to_response('elements\manager_parents_answers.html', {
        'answers': answers
    })


# Сохранение заключения
def save_conclusion(request):
    p_test = TestPassing.objects.get(pk=request.POST.get('p_test_id'))

    conclusion = request.POST.get('conclusion')
    p_test.conclusion = conclusion
    p_test.save()

    return HttpResponse(status=204)


# Страница сравнения тестов
def tests_compare(request, t_id=1, user_id=1):
    user = auth.get_user(request)
    if user.is_anonymous or not user.profile.is_manager():
        return render_to_response('tests_compare.html', {
            'results': ''
        })

    # Получаем id теста из TestPassing
    test_id = TestPassing.objects.filter(pk=t_id).last().test_id

    # Берём все прохождения тестов юзером
    test_passing = TestPassing.objects.filter(user_id=user_id, pk__lte=t_id, test_id=test_id).exclude(
        test_complete=False).order_by('date_pass').reverse()

    # Формируем список тестов для выбора в сравнении
    initial_tests = list(map(lambda p_test: p_test.pk, test_passing[:5]))

    tests_passing_for_select = {}
    for test in test_passing:
        varp = {test.pk: test.date_pass}
        tests_passing_for_select[test] = varp

    return render_to_response('tests_compare.html',
                              {'test_passing_for_select': tests_passing_for_select, 'initial_tests': initial_tests,
                               'username': user.profile.name_with_abbreviation(), 'is_manager': True, 't_id': t_id})


def results_for_compare(request):
    tests = request.POST.getlist('tests[]')
    test_passing = TestPassing.objects.filter(pk__in=tests)
    test_id = test_passing.last().test_id
    user_id = test_passing.last().user_id

    # Формируем лист результатов
    calc_results = {}
    results = CalcResult.objects.filter(test_id=test_id)

    for result in results:
        calc_result = []
        for p_test in tests:
            calc_result.append(
                '{}'.format(culc_result(result.formula, test_id, p_test, User.objects.get(pk=user_id), result)))
        calc_results['{} ({})'.format(result.label, result.annotation)] = calc_result

    return render_to_response('elements/test_compare_results.html',
                              {'results': calc_results, 'tests_passing': test_passing, 't_count': len(tests)})


def conclusions_for_compare(request):
    tests = request.POST.getlist('tests[]')
    test_passing = TestPassing.objects.filter(pk__in=tests)

    # Формируем лист заключений
    conclusions = list(map(lambda p_test: TestPassing.objects.get(pk=p_test).conclusion, tests))

    return render_to_response('elements/test_compare_conclusions.html',
                              {'conclusions': conclusions, 'tests_passing': test_passing})


def answers_for_compare(request):
    tests = request.POST.getlist('tests[]')
    test_passing = TestPassing.objects.filter(pk__in=tests)

    # Берём ответы на вопросы включая только ответы с test_id из предыдущего листа, сортируя по номеру вопроса и test_id
    result_on_question = ResultOnQuestion.objects.filter(test_id__in=tests).order_by(
        'question__question__number').order_by('test_id').reverse()

    # Создаём лист из QuerySet result_on_question чтобы можно было изменять значения и они сохранились при отправке
    answers = list(result_on_question)
    # Берём первый элемент как "предыдущий", чтобы не было ошибки на первой итерации цикла
    prev_answer = answers[0]

    # Формируем лист вопросов
    for answer in answers[1:]:
        # Если номер текущего вопроса и номер предыдущего вопроса совпадает, значит имеем дело с вопросом
        # с множественными ответами и в этом случае добавляем к description ответа значение description предыдущего
        # ответа и удаляем его(чтобы не отображался)

        if answer.question_id == prev_answer.question_id and answer.test_id == prev_answer.test_id:
            answer.answer.description = '{} ({}), {}'.format(prev_answer.answer.description, prev_answer.ball,
                                                             answer.answer.description)
            answers.remove(prev_answer)

        # Сохраняем текущее значение вопроса для следующей итерации цикла
        prev_answer = answer

    return render_to_response('elements/test_compare_answers.html',
                              {'answers': answers, 't_count': len(tests), 'tests_passing': test_passing})

# -*- coding: utf-8 -*-
import logging
import re

from test_module.models import ResultOnQuestion, TestPassing, Question, TeacherTest
from user_profile.models import Institution
from users_relations.models import UsersRelations, RelationType

logger = logging.getLogger(__name__)


def get_manager_test_filter(request):
    """Формирует из параметров запроса параметры WHERE SQL-запроса для таблицы test_passed"""

    # TODO до внедрения логирования пока так
    print('Параметры фильтрации списка прошедших тест:')
    test_id = request.POST.get('test_id')
    print('test_id = {}'.format(test_id))
    first_name = request.POST.get('first_name')
    print('first_name = {}'.format(first_name))
    second_name = request.POST.get('second_name')
    print('second_name = {}'.format(second_name))
    school = request.POST.get('school')
    print('school = {}'.format(school))
    school_class = request.POST.get('school_class')
    print('school_class = {}'.format(school_class))

    # TODO добавить проверку наличия этиго параметра, так как это обязательный параметр
    filter_params = {
        'test_id': test_id
    }

    if first_name:
        filter_params['user__first_name__icontains'] = first_name.strip()
    if second_name:
        filter_params['user__last_name__icontains'] = second_name.strip()
    if school:
        school = Institution.objects.get(institution_name=school).pk
        filter_params['user__profile__institution__id__icontains'] = school
    if school_class:
        filter_params['user__profile__school_class__icontains'] = school_class.strip()

    return filter_params


def find_cell(formula_str):
    sys_delimiter = "$"
    start = formula_str.index(sys_delimiter)
    end = formula_str[start + 1:].index(sys_delimiter) + start + 1
    substring = formula_str[start:end + 1]
    logger.debug("source string = {}".format(formula_str))
    logger.debug("start index = {}".format(start))
    logger.debug("end index = {}".format(end))
    logger.debug("substring = {}".format(substring))
    return substring


def normalize_formula(formula_str):
    return formula_str.replace(" ", "")


def culc_result(formula, test_id, p_test_id, user, formulas):
    # Получаю все вхождения в формуле типа "вт, вр, ву"
    answers = re.findall(r'\w+', formula)
    # Для каждого из вхождений получаю вес ответа из таблицы result_on_question
    for answer in answers:
        weight = get_number(answer, test_id, p_test_id, user)
        if weight == 'Родитель':
            return 'Родитель ещё не прошел тест'
        elif weight == 'Учитель':
            return 'Учитель ещё не прошел тест'
        elif weight == 'Вопрос':
            return 'В формуле указан несуществующий вопрос {}'.format(answer)
        elif weight == 'Формула':
            return 'Ошибка: {} - недопустимый член выражения(проверьте формулу)'.format(answer)
        elif weight == 'Оба':
            return 'Родитель и учитель ещё не прошли тестирование'
        else:
            formula = re.sub(answer, weight, formula, 1)
    return eval(formula)


# функция получения веса ответа
def get_weight(question_id, test_id, p_test_id):
    if not Question.objects.filter(test_id=test_id, number=question_id).exists():
        return 'Вопрос'
    question = Question.objects.get(test_id=test_id, number=question_id)
    answers = ResultOnQuestion.objects.filter(test_id=p_test_id, question_id=question.question_desc.pk)
    result = 0

    for answer in answers:
        result += answer.ball
    return str(result)


# функция получения номера ответа
def get_number(answer, test_id, p_test_id, user):
    # из полученной строки типа "втX, врХ, вуХ" - где X число, получаю число
    find = re.search(r'\w\w', answer)

    if answer.isdigit():
        return answer

    # тестируемый
    elif find.group(0) == 'вт':
        # Получаем номер вопроса
        question_number = re.search(r"\d+", answer).group(0)

        # Получаем вес ответа из объекта модели
        weight = get_weight(question_number, test_id, p_test_id)
        return weight
    # родитель
    elif find.group(0) == 'вр':
        parent_type = re.search(r"\d", answer).group(0)
        question_number = re.search(r"\d+", answer).group(0)[1:]
        relation_type = RelationType.objects.get(relation='Родитель-Ребенок')
        # Если в таблице нету результатов прохождения родителем теста - указывает что родитель не прошел тест
        if not TestPassing.objects.filter(
                user_id=UsersRelations.objects.filter(relation_type=relation_type.pk,
                                                      user_id=user.pk).last().user_second_id).exists():
            if not TeacherTest.objects.filter(
                    user_id=UsersRelations.objects.filter(
                        relation_type=RelationType.objects.get(relation='Учитель-ученик').pk,
                        user_id=user.pk).user_second_id, student_id=user.pk).exists():
                return 'Оба'
            return 'Родитель'
        # Получаяем id прохождения теста родителя
        # TODO Сделать проверку на существование двух родителей (вынести UsersRelations.objects.get в отдельную
        # TODO переменную и проходить циклом по ним и плюсовать полученные weight
        if parent_type == 1:
            if UsersRelations.objects.filter(relation_type=relation_type.pk, user_id=user.pk,
                                             user_second__is_woman=True).exists():
                parent = UsersRelations.objects.get(relation_type=relation_type.pk, user_id=user.pk,
                                                    user_second__is_woman=True).user_second
            else:
                return '0'
        else:
            if UsersRelations.objects.filter(relation_type=relation_type.pk, user_id=user.pk,
                                             user_second__is_woman=False).exists():
                parent = UsersRelations.objects.get(relation_type=relation_type.pk, user_id=user.pk,
                                                    user_second__is_woman=False).user_second
            else:
                return '0'
        p_test_id = TestPassing.objects.filter(user_id=parent.pk).last()
        weight = get_weight(question_number, p_test_id.test_id, p_test_id.pk)
        return weight

    # учитель
    elif find.group(0) == 'ву':
        question_number = re.search(r"\d+", answer).group(0)
        relation_type = RelationType.objects.get(relation='Учитель-ученик')
        # Если в таблице нету результатов прохождения тестов учителем - указывает что учитель не прошел тест
        if not TeacherTest.objects.filter(
                teacher_id=UsersRelations.objects.get(relation_type=relation_type.pk,
                                                      user_id=user.pk).user_second_id, student_id=user.pk).exists():
            if not TestPassing.objects.filter(
                    user_id=UsersRelations.objects.filter(
                        relation_type=RelationType.objects.get(relation='Родитель-Ребенок').pk,
                        user_id=user.pk)).exists():
                return 'Оба'
            return 'Учитель'
        # Получаяем id прохождения теста учителяссссс
        p_test_id = TeacherTest.objects.filter(
            teacher_id=UsersRelations.objects.get(relation_type=relation_type.pk,
                                                  user_id=user.pk).user_second_id).last()
        weight = get_weight(question_number, p_test_id.passing_test.test_id, p_test_id.pk)
        return weight
    else:
        return 'Формула'

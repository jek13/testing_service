from django.apps import AppConfig


class TestModuleConfig(AppConfig):
    name = 'test_module'

from django.conf.urls import url

from test_module import views

urlpatterns = [
    url(r'^testable/(?P<t_id>\d+)/$', views.testable),
    url(r'^my/', views.tests),
    url(r'^all/', views.manager_tests),
    url(r'^management/', views.management),
    url(r'^passed/(?P<t_id>\d+)/$', views.passed),
    url(r'^info/(?P<t_id>\d+)/$', views.rez_info),
    url(r'^ajax_passed_filter/', views.ajax_passed_filter),
    url(r'^next-question/', views.next_question),
    url(r'^test-complete/(?P<t_id>\d+)/$', views.test_complete),
    url(r'^create/', views.create),
    url(r'^ajax_answers/', views.ajax_answers),
    url(r'^ajax_result/', views.ajax_result),
    url(r'^ajax_relation_answer/', views.ajax_relation_answer),
    url(r'^save_conclusion/', views.save_conclusion),
    url(r'compare/(?P<t_id>\d+)/(?P<user_id>\d+)/$', views.tests_compare),
    url(r'answers_for_compare/$', views.answers_for_compare),
    url(r'results_for_compare/$', views.results_for_compare),
    url(r'conclusions_for_compare/$', views.conclusions_for_compare)
]

import unittest

from test_module.util import find_cell, normalize_formula


class UtilUnitTest(unittest.TestCase):

    def test_parse_formula(self):
        rez1 = find_cell("13$T1$123")
        self.assertEqual("$T1$", rez1)

        rez2 = find_cell("$T1$")
        self.assertEqual("$T1$", rez2)

        rez3 = find_cell("$T1$123131")
        self.assertEqual("$T1$", rez3)

        rez4 = find_cell("$T1$+$T2$")
        self.assertEqual("$T1$", rez4)

    def test_normalize_formula(self):
        rez1 = normalize_formula("  1 3  $T  1  $    1 23    ")
        self.assertEqual("13$T1$123", rez1)

# -*- coding: utf-8 -*-
from django.contrib.auth.models import User
from django.db import models


class Test(models.Model):
    """Информация о тесте"""

    class Meta:
        db_table = 'test'
        verbose_name = 'Тест'
        verbose_name_plural = 'Тесты'

    name = models.CharField(verbose_name="Наименование", max_length=50)
    description = models.TextField(verbose_name="Описание", null=True, blank=True)
    time_to_pass = models.IntegerField(verbose_name="Время для прохождения", null=True, blank=True)
    is_active = models.BooleanField(verbose_name="Доступен для прохождения", default=False)

    def __str__(self):
        return "{}, доступен: {}, время для прохождения (в секундах): {}".format(self.name,
                                                                                 "да" if self.is_active else "нет",
                                                                                 self.time_to_pass if self.time_to_pass is not None else "не определено")


class TestPassing(models.Model):
    """Прохождение пользователем теста"""

    class Meta:
        db_table = 'test_passing'
        verbose_name = 'Прохождение теста'
        verbose_name_plural = 'Пройденные тесты'

    test = models.ForeignKey(Test, blank=True, null=True, verbose_name="Тест", on_delete=models.CASCADE)
    user = models.ForeignKey(User, blank=True, null=True, verbose_name="Пользователь", on_delete=models.CASCADE,
                             default=1)
    date_pass = models.DateTimeField(verbose_name="Дата прохождения")
    time_pass = models.IntegerField(verbose_name="Время прохождения", null=True, blank=True)
    again_pass = models.BooleanField(verbose_name="Повторное прохождение", default=False)
    conclusion = models.TextField(verbose_name="Заключение", null=True, blank=True)
    test_complete = models.BooleanField(verbose_name='Тест завершен', default=False)

    def __str__(self):
        return 'Прохождение теста "{}", пользователем {} - {}'.format(self.test.name,
                                                                      self.user.profile.name_with_abbreviation(),
                                                                      self.date_pass)


class TeacherTest(models.Model):
    """Таблица сохранения пройденных тестов учителем"""

    class Meta:
        db_table = 'teacher_tests'
        verbose_name = 'Прохождение теста учителем'
        verbose_name_plural = 'Прохождения тестов учителем'

    passing_test = models.ForeignKey(TestPassing, verbose_name='Прохождение теста', on_delete=models.CASCADE)
    teacher = models.ForeignKey(User, verbose_name='Учитель', related_name='teacher +', on_delete=models.CASCADE)
    student = models.ForeignKey(User, verbose_name='Ученик', related_name='student +', on_delete=models.CASCADE)

    def __str__(self):
        return 'Прохождения теста учителем {} для ученика {}'.format(self.teacher.profile.name_with_abbreviation(),
                                                                     self.student.profile.name_with_abbreviation())


class QuestionDesc(models.Model):
    """Вопрос теста"""

    class Meta:
        db_table = 'questions_desc'
        verbose_name = 'Формулировка вопроса'
        verbose_name_plural = 'Формулировки вопросов'

    question_desc = models.TextField(verbose_name='Формулировка вопроса')
    answer_number = models.BooleanField(default=False, verbose_name='Несколько ответов')

    def __str__(self):
        return self.question_desc


class AnswerOnQuestion(models.Model):
    """Вариант ответа на вопроса"""

    class Meta:
        db_table = 'answer_on_question'
        verbose_name = 'Формулировка ответа'
        verbose_name_plural = 'Формулировки ответа'

    description = models.CharField(verbose_name='Формулировка', blank=True, null=True, max_length=200)

    def __str__(self):
        return self.description


class Answer(models.Model):
    """Варианты ответов на вопросы"""

    class Meta:
        db_table = 'answer'
        verbose_name = 'Вариант ответа на вопрос'
        verbose_name_plural = 'Варианты ответов на вопрос'
        unique_together = ('answer_question', 'number')

    answer_question = models.ForeignKey(QuestionDesc, null=True, blank=True, verbose_name='Вопрос',
                                        on_delete=models.CASCADE)
    answer = models.ForeignKey(AnswerOnQuestion, null=True, blank=True, verbose_name='Ответ', on_delete=models.CASCADE)
    answer_weight = models.IntegerField(verbose_name='Вес ответа', default=1)
    number = models.IntegerField(verbose_name='Порядковый номер ответа', default=1)

    def __str__(self):
        return 'Вопрос: {}. Формулировка ответа:{}. Вес ответа: {}. Номер ответа: {}'.format(
            self.answer_question.question_desc, self.answer.description, self.answer_weight, self.number)


class Question(models.Model):
    """Вопросы, которые учавствуют в тестировании"""

    class Meta:
        db_table = 'questions'
        verbose_name = 'Вопрос теста'
        verbose_name_plural = 'Вопросы теста'
        unique_together = ('test', 'number')

    test = models.ForeignKey(Test, blank=True, null=True, verbose_name='Тест', on_delete=models.CASCADE)
    question_desc = models.ForeignKey(QuestionDesc, blank=True, null=True, verbose_name='Формулировка',
                                      on_delete=models.CASCADE)
    number = models.IntegerField(verbose_name='Порядковый номер вопроса', default=1)

    def __str__(self):
        return 'Тест: {}, Вопрос: {}. Порядковый номер вопроса: {}'.format(self.test.name,
                                                                           self.question_desc.question_desc,
                                                                           self.number)


class ResultOnQuestion(models.Model):
    """Результат ответа на вопрос тестирования"""

    class Meta:
        db_table = 'result_on_question'
        verbose_name = 'Конечный ответ'
        verbose_name_plural = 'Конечные ответы'

    test = models.ForeignKey(TestPassing, verbose_name='Тест', blank=True, null=True, on_delete=models.CASCADE)
    question = models.ForeignKey(QuestionDesc, verbose_name='Формулировка вопроса', blank=True, null=True,
                                 on_delete=models.CASCADE)
    answer = models.ForeignKey(AnswerOnQuestion, verbose_name='Формулировка ответа', blank=True, null=True,
                               on_delete=models.CASCADE)
    ball = models.IntegerField(verbose_name='Балл', default=1)

    def __str__(self):
        return 'Результаты ответа на тест {} пользователем {}. Вопрос: {}, ответ: {}. Бал: {}'.format(
            self.test.test.name,
            self.test.user.profile.name_with_abbreviation(),
            self.question.question_desc,
            self.answer.description,
            self.ball)


class CalcResult(models.Model):
    """Расчет результатов тестирования"""

    class Meta:
        db_table = 'calc_result'
        verbose_name = 'Результат тестирования'
        verbose_name_plural = 'Результаты тестирования'

    test = models.ForeignKey(Test, verbose_name='Тест', on_delete=models.CASCADE)
    formula = models.CharField(verbose_name='Формула расчета', max_length=256)
    label = models.CharField(verbose_name='Наименование поля', blank=True, null=True, max_length=256)
    annotation = models.CharField(verbose_name='Пояснение по результатам', max_length=128, blank=True, null=True)

    def __str__(self):
        return 'Замена макроса "{}" значением "{}" для теста: {}'.format(self.macro, self.formula, self.test.name)

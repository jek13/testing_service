from django.contrib import admin
from test_module.models import *


class TestAdmin(admin.ModelAdmin):
    list_filter = ['is_active']


class TestPassingAdmin (admin.ModelAdmin):
    list_filter = ['test', 'user', 'date_pass']


class AnswerOnQuestionInlines(admin.TabularInline):
    model = AnswerOnQuestion
    extra = 1


class AnswerAdmin(admin.ModelAdmin):
    inlines = [AnswerOnQuestionInlines]


class CalcResultAdmin(admin.ModelAdmin):
    list_filter = ['test']


admin.site.register(Test, TestAdmin)
admin.site.register(TestPassing, TestPassingAdmin)
admin.site.register(Question)
admin.site.register(QuestionDesc)
admin.site.register(Answer)
admin.site.register(ResultOnQuestion)
admin.site.register(AnswerOnQuestion)
admin.site.register(CalcResult, CalcResultAdmin)

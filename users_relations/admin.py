from django.contrib import admin

from users_relations.models import *


admin.site.register(AllowedTypes)
admin.site.register(RelationType)
admin.site.register(UsersRelations)

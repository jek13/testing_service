from django.db import models
from user_profile.models import Profile
from test_module.models import Test
from django.contrib.auth.models import Group


class AllowedTypes(models.Model):
    '''Тестируемые типы'''

    class Meta:
        db_table = 'allowed_types'
        verbose_name = 'Тестируемый тип'
        verbose_name_plural = 'Тестируемые типы'

    user_type = models.ForeignKey(Group, blank=True, null=True, verbose_name="Тип пользователя",
                                  on_delete=models.CASCADE)
    test = models.ForeignKey(Test, blank=True, null=True, verbose_name="Тест", on_delete=models.CASCADE)

    def __str__(self):
        return self.user_type.name


class RelationType(models.Model):
    '''Тип связи'''

    class Meta:
        db_table = 'relation_type'
        verbose_name = 'Тип связи'
        verbose_name_plural = 'Типы связей'

    relation = models.CharField(verbose_name='Связь', null=False, blank=True, max_length=20)
    description = models.TextField(verbose_name='Описание')

    def __str__(self):
        return self.description


class UsersRelations(models.Model):
    '''Взаимосвязь тестируемых'''

    class Meta:
        db_table = 'users_relations'
        verbose_name = 'Связанных пользователей'
        verbose_name_plural = 'Отношения пользователей'

    user = models.ForeignKey(Profile, related_name="first_user +", on_delete=models.CASCADE)
    user_second = models.ForeignKey(Profile, related_name="second_user +", on_delete=models.CASCADE)
    relation_type = models.ForeignKey(RelationType, blank=True, null=True, verbose_name="Тип связи",
                                      on_delete=models.CASCADE)

    def __str__(self):
        return '{} {}'.format(self.user, self.user_second)

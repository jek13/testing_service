from django.apps import AppConfig


class UsersRelationsConfig(AppConfig):
    name = 'users_relations'

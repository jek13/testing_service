# -*- coding: utf-8 -*-
from django.contrib import auth  # created in step
from django.contrib.auth.models import Group
from django.http import HttpResponse
from django.shortcuts import redirect
from django.template.loader import get_template

from test_module.models import CalcResult, TestPassing
from test_module.util import culc_result
from users_relations.models import UsersRelations, RelationType, AllowedTypes

from .utils import render_to_pdf


def get(request, test_id=1):
    template = get_template('invoice.html')

    user_result = []
    teacher_result = []
    parent_result = []

    user_p_test = TestPassing.objects.get(pk=test_id)

    user = user_p_test.user

    user_formuls = CalcResult.objects.filter(
        test__allowedtypes=AllowedTypes.objects.get(user_type=Group.objects.get(name='Учащиеся')),
        test_id=user_p_test.test_id)

    for label in user_formuls:
        if label.formula.__contains__('вт'):
            user_result.append(
                '{} - {} ({})'.format(label.label,
                                      culc_result(label.formula, user_p_test.test_id, user_p_test, user_p_test.user,
                                                  label), label.annotation))
        elif label.formula.__contains__('вр'):
            parent_result.append(
                '{} - {} ({})'.format(label.label,
                                      culc_result(label.formula, user_p_test.test_id, user_p_test, user_p_test.user,
                                                  label), label.annotation))
        elif label.formula.__contains__('ву'):
            teacher_result.append(
                '{} - {} ({})'.format(label.label,
                                      culc_result(label.formula, user_p_test.test_id, user_p_test, user_p_test.user,
                                                  label), label.annotation))

    context = {
        "user_result": user_result,
        "teacher_result": teacher_result,
        "parent_result": parent_result,
        "user": user
    }
    html = template.render(context)
    pdf = render_to_pdf('invoice.html', context)
    user = auth.get_user(request)
    if user.is_anonymous:
        return redirect('/')
    return HttpResponse(pdf, content_type='application/pdf')

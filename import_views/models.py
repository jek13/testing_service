from django.contrib.auth.models import User
from django.db import models


class ImportHistory(models.Model):
    """Таблица истории импорта"""

    class Meta:
        db_table = 'import_history'
        verbose_name = 'История импорта'
        verbose_name_plural = 'Истории импорта'

    user = models.ForeignKey(User, blank=True, null=False, verbose_name="Пользователь", on_delete=models.CASCADE,
                             default=1)
    file_name = models.CharField(verbose_name="Имя файла", max_length=128)
    rows = models.IntegerField(verbose_name='Количество строк в файле', default=1)
    import_start = models.DateTimeField(verbose_name="Время начала импорта", auto_now=True)
    import_over = models.DateTimeField(verbose_name="Время окончания импорта", auto_now=False, null=True)
    status = models.CharField(verbose_name="Статус импорта", max_length=32, null=True)

    def __str__(self):
        return 'Импорт файла: {}'.format(self.file_name)


class Log(models.Model):
    """Таблица логов"""

    class Meta:
        db_table = 'log'
        verbose_name = 'История логов'
        verbose_name_plural = 'Истории логов'

    import_history = models.ForeignKey(ImportHistory, verbose_name="История импорта", on_delete=models.CASCADE)
    status = models.CharField(verbose_name="Статус импорта", max_length=32)
    comment = models.CharField(verbose_name="Комментарии", max_length=255)
    add_date = models.DateTimeField(verbose_name="Дата добавления", auto_now=True)

from django.conf.urls import url

from import_views import views

urlpatterns = [
    url(r'^logs/(?P<import_history_id>\d+)/$', views.get_logs_by_import_history),
    url(r'^history/', views.get_history),
    url(r'^progress/', views.get_progress),
    url(r'^time', views.get_time)
]

from django.contrib import admin

from import_views.models import ImportHistory, Log

admin.site.register(ImportHistory)
admin.site.register(Log)

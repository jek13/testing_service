from django.apps import AppConfig


class ImportViewsConfig(AppConfig):
    name = 'import_views'

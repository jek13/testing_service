import datetime

from django.contrib import auth
from django.http import HttpResponse
from django.shortcuts import render_to_response, redirect

# Create your views here.
from import_views.models import Log, ImportHistory


def get_current_import(time):
    return Log.objects.filter(add_date__gte=time)


def get_progress(request):
    current_import = get_current_import(request.POST.get('time'))
    if current_import:
        current = len(current_import)
        rows = current_import.last().import_history.rows
    else:
        return render_to_response('progress.html', {})

    params = {
        'current': current,
        'all': rows,
        'progress': int(int(current) / rows * 100)
    }

    return render_to_response('progress.html', params)


def get_history(request):
    import_history_all = ImportHistory.objects.all().order_by('import_start').reverse()[:15]
    return render_to_response('history.html', {'import_history': import_history_all})


def get_time(request):
    return HttpResponse(datetime.datetime.now(), content_type='text/html')


def get_logs_by_import_history(request, import_history_id=1):
    user = auth.get_user(request)
    if user.is_anonymous or not user.profile.is_manager():
        return redirect('/')

    import_history = ImportHistory.objects.get(pk=import_history_id)
    logs = Log.objects.filter(import_history=import_history)

    params = {
        'logs': logs,
        'import_history': import_history,
        'is_manager': True,
        'username': user.profile.name_with_abbreviation()
    }

    return render_to_response('logs.html', params)

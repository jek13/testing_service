"""testing_service URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls import include, url
from django.contrib import admin

from file_import import urls as upload_urls
from import_views import urls as import_urls
from loginsys import urls as login_urls
from test_module import urls
from user_profile import urls as users_urls

urlpatterns = [
    url(r'^download/(?P<test_id>\d+)/', include('pdf_report.urls')),
    url(r'upload/', include(upload_urls)),
    url(r'^import/', include(import_urls)),
    url(r'^admin/', admin.site.urls),
    url(r'^test/', include(urls)),
    url(r'^users/', include(users_urls)),
    url(r'^', include(login_urls)),
]

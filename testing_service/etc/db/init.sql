-- Группы
INSERT INTO auth_group (id, name) VALUES (1, 'Учащиеся');
INSERT INTO auth_group (id, name) VALUES (2, 'Преподаватели');
INSERT INTO auth_group (id, name) VALUES (3, 'Родители');
INSERT INTO auth_group (id, name) VALUES (4, 'Менеджеры');

-- Пользователи (пароль общий для всех 123qwerty)
INSERT INTO auth_user (id, password, last_login, is_superuser, username, first_name, email, is_staff, is_active, date_joined, last_name) VALUES (2, 'pbkdf2_sha256$100000$sp25O0KtGBy6$CMfu6hYpMKup2MfBuL/127ckoSMZAE9x308IOxXiShM=', null, 0, 'stud1', 'Иван', '', 0, 1, '2018-04-06 17:03:15', 'Иванов');
INSERT INTO auth_user (id, password, last_login, is_superuser, username, first_name, email, is_staff, is_active, date_joined, last_name) VALUES (3, 'pbkdf2_sha256$100000$eAdispeux4bT$hjGZ58LP+S7t8HH2flUBF43zSz3ozu+BRZCfXUu5yEY=', null, 0, 'stud2', 'Петр', '', 0, 1, '2018-04-06 17:05:10', 'Петров');
INSERT INTO auth_user (id, password, last_login, is_superuser, username, first_name, email, is_staff, is_active, date_joined, last_name) VALUES (4, 'pbkdf2_sha256$100000$77TtBLBKuRn0$eMU5ABDfvmZY5O3A3hq/o/TTFsTr6WI3AtXOjWHv70I=', null, 0, 'stud3', 'Владимир', '', 0, 1, '2018-04-06 17:05:47', 'Владимирович');
INSERT INTO auth_user (id, password, last_login, is_superuser, username, first_name, email, is_staff, is_active, date_joined, last_name) VALUES (5, 'pbkdf2_sha256$100000$S2yvYAxA48j4$CmzVsyXDeYqzf8Tt5oyfTNuBfW8y9+3U+IYmj9Ww+so=', null, 0, 'rod1', 'Наталья', '', 0, 1, '2018-04-06 17:28:06', 'Петрович');
INSERT INTO auth_user (id, password, last_login, is_superuser, username, first_name, email, is_staff, is_active, date_joined, last_name) VALUES (6, 'pbkdf2_sha256$100000$78yMNs2x3N0F$28PesHdjxaeLZFv4e7/8BPc00I9hnIMTW0awFz8NX78=', null, 0, 'Tech1', 'Марфа', '', 0, 1, '2018-04-06 17:29:20', 'Гвоздь');

-- Учебные заведения
INSERT INTO institutions (id, institution_name, additional_information) VALUES (1, 'Школа №1', 'Тестовая школа №1');
INSERT INTO institutions (id, institution_name, additional_information) VALUES (2, 'Школа №33', 'Тестовая школа №33');

-- Профили
INSERT INTO user_profile (id, patronymic, birth_date, is_woman, user_id, school_class, institution_id) VALUES (2, 'Иванович', '2000-11-01', 0, 2, '11А', 1);
INSERT INTO user_profile (id, patronymic, birth_date, is_woman, user_id, school_class, institution_id) VALUES (3, 'Петрович', '2000-12-11', 0, 3, '11Б', 1);
INSERT INTO user_profile (id, patronymic, birth_date, is_woman, user_id, school_class, institution_id) VALUES (4, 'Владимирович', '2001-01-11', 0, 4, '10Б', 2);
INSERT INTO user_profile (id, patronymic, birth_date, is_woman, user_id, school_class, institution_id) VALUES (5, 'Анатольевна', '1970-11-11', 1, 5, null, null);
INSERT INTO user_profile (id, patronymic, birth_date, is_woman, user_id, school_class, institution_id) VALUES (6, 'Ивановна', '1980-11-11', 1, 6, null, null);

-- Группы пользователей
INSERT INTO auth_user_groups (id, user_id, group_id) VALUES (1, 1, 4);
INSERT INTO auth_user_groups (id, user_id, group_id) VALUES (2, 5, 3);
INSERT INTO auth_user_groups (id, user_id, group_id) VALUES (3, 6, 2);
INSERT INTO auth_user_groups (id, user_id, group_id) VALUES (4, 2, 1);
INSERT INTO auth_user_groups (id, user_id, group_id) VALUES (5, 3, 1);
INSERT INTO auth_user_groups (id, user_id, group_id) VALUES (6, 4, 1);

-- Тесты
INSERT INTO test (id, name, description, time_to_pass, is_active) VALUES (1, 'Тест 1', 'Первый тестовый тест.', null, 1);
INSERT INTO test (id, name, description, time_to_pass, is_active) VALUES (2, 'Тест 2', 'Второй тестовый тест, не доступен для прохождения', null, 0);
INSERT INTO test (id, name, description, time_to_pass, is_active) VALUES (3, 'Тест 3', 'Еще тестовый тест. Указано время прохолждения.', 18000, 1);

-- Типы связей между пользователями
INSERT INTO relation_type (id, relation, description) VALUES (1, 'Родитель', 'Пользователь является родителем');
INSERT INTO relation_type (id, relation, description) VALUES (2, 'Преподаватель', 'Пользователь является преподавателем');

-- Отношения пользователей
INSERT INTO users_relations (id, relation_type_id, user_id, user_second_id) VALUES (1, 1, 2, 5);
INSERT INTO users_relations (id, relation_type_id, user_id, user_second_id) VALUES (2, 1, 2, 6);

-- Тесты доступные для типов
INSERT INTO allowed_types (id, test_id, user_type_id) VALUES (1, 1, 1);
INSERT INTO allowed_types (id, test_id, user_type_id) VALUES (2, 3, 1);

--Варианты вопросов теста
INSERT INTO questions_desc (id, question_desc, answer_number) VALUES (1, 'Я дерусь не реже и не чаще, чем другие.', 0);
INSERT INTO questions_desc (id, question_desc, answer_number) VALUES (2, 'Я чувствую себя одиноким с людьми.', 1);
INSERT INTO questions_desc (id, question_desc, answer_number) VALUES (3, 'У меня нет друзей.', 0);
INSERT INTO questions_desc (id, question_desc, answer_number) VALUES (4, 'Обычно Я спокоен', 0);
INSERT INTO questions_desc (id, question_desc, answer_number) VALUES (5, 'Обычно Я доволен собой', 0);
INSERT INTO questions_desc (id, question_desc, answer_number) VALUES (6, 'Меня волнуют возможные неудачи', 0);

--Варианты ответов на вопрос
INSERT INTO answer_on_question (id, description) VALUES (1, 'Да');
INSERT INTO answer_on_question (id, description) VALUES (2, 'Нет');
INSERT INTO answer_on_question (id, description) VALUES (3, 'Не знаю');
INSERT INTO answer_on_question (id, description) VALUES (4, 'Почти всегда');
INSERT INTO answer_on_question (id, description) VALUES (5, 'Часто');
INSERT INTO answer_on_question (id, description) VALUES (6, 'Никогда');

--Связь вопросов и ответов
INSERT INTO answer (id, answer_weight, answer_id, answer_question_id, number) VALUES (1, 1, 1, 1, 1);
INSERT INTO answer (id, answer_weight, answer_id, answer_question_id, number) VALUES (2, 0, 2, 1, 2);
INSERT INTO answer (id, answer_weight, answer_id, answer_question_id, number) VALUES (3, 1, 1, 2, 1);
INSERT INTO answer (id, answer_weight, answer_id, answer_question_id, number) VALUES (4, 0, 2, 2, 1);
INSERT INTO answer (id, answer_weight, answer_id, answer_question_id, number) VALUES (5, 1, 1, 3, 1);
INSERT INTO answer (id, answer_weight, answer_id, answer_question_id, number) VALUES (6, 0, 2, 3, 2);
INSERT INTO answer (id, answer_weight, answer_id, answer_question_id, number) VALUES (7, -1, 3, 3, 2);
INSERT INTO answer (id, answer_weight, answer_id, answer_question_id, number) VALUES (8, 0, 4, 4, 1);
INSERT INTO answer (id, answer_weight, answer_id, answer_question_id, number) VALUES (9, 2, 5, 4, 2);
INSERT INTO answer (id, answer_weight, answer_id, answer_question_id, number) VALUES (10, 3, 6, 4, 3);
INSERT INTO answer (id, answer_weight, answer_id, answer_question_id, number) VALUES (11, 1, 4, 5, 1);
INSERT INTO answer (id, answer_weight, answer_id, answer_question_id, number) VALUES (12, 2, 5, 5, 2);
INSERT INTO answer (id, answer_weight, answer_id, answer_question_id, number) VALUES (13, 3, 6, 5, 3);
INSERT INTO answer (id, answer_weight, answer_id, answer_question_id, number) VALUES (14, 1, 4, 6, 1);
INSERT INTO answer (id, answer_weight, answer_id, answer_question_id, number) VALUES (15, 2, 5, 6, 2);
INSERT INTO answer (id, answer_weight, answer_id, answer_question_id, number) VALUES (16, 3, 6, 6, 3);

--Связь ткстов и вопросов
INSERT INTO questions (id, number, question_desc_id, test_id) VALUES (1, 6, 1, 3);
INSERT INTO questions (id, number, question_desc_id, test_id) VALUES (2, 5, 2, 3);
INSERT INTO questions (id, number, question_desc_id, test_id) VALUES (3, 3, 3, 3);
INSERT INTO questions (id, number, question_desc_id, test_id) VALUES (4, 1, 4, 3);
INSERT INTO questions (id, number, question_desc_id, test_id) VALUES (5, 2, 5, 3);
INSERT INTO questions (id, number, question_desc_id, test_id) VALUES (6, 4, 6, 3);
from django.contrib import auth
from django.http import HttpResponse
from django.shortcuts import redirect
# Create your views here.
from django.utils.datetime_safe import datetime

from file_import import file_import
from import_views.models import ImportHistory
from test_module.forms import UploadFileForm


def upload(request):
    user = auth.get_user(request)
    if user.is_anonymous or not user.profile.is_manager():
        return redirect('/')

    if request.is_ajax():
        form = UploadFileForm(request.POST, request.FILES)
        if form.is_valid():
            file = request.FILES['file']
            import_history, created = ImportHistory.objects.get_or_create(user=user, file_name=file.name,
                                                                          import_start=datetime.now())
            file_import.upload_file(file, import_history)

    return HttpResponse(status=201)

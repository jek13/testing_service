import csv
import datetime
import re

from django.core.exceptions import ObjectDoesNotExist

from import_views.models import Log
from test_module.models import CalcResult, Test


def import_formuls(path, import_history):
    import_history.rows = len(open(path).readlines()) - 1
    import_history.save()
    with open(path) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=';')
        next(csv_reader)
        for row in csv_reader:
            # Формула
            formula = row[0]
            is_f_valid = is_valid(formula)

            # Наименование поля
            label = row[1]

            # Пояснение к результатам
            annotation = row[2]

            # Тест, для которого формула
            try:
                test = Test.objects.get(name=row[3])
            except ObjectDoesNotExist:
                Log.objects.get_or_create(import_history=import_history, status='Ошибка',
                                          comment='Такого теста не существует: {}. Сначала импортируйте тест!'.format(
                                              row[3]))
                import_history.import_over = datetime.datetime.now()
                import_history.status = 'Ошибка'
                import_history.save()

                return

            if is_f_valid is None:
                calc_result, created = CalcResult.objects.update_or_create(label=label, test=test,
                                                                           defaults={'formula': formula,
                                                                                     'annotation': annotation})
                if created:
                    Log.objects.get_or_create(import_history=import_history, status='Успешно',
                                              comment='Добавлена формула {} ({}) для теста {}'.format(label, formula,
                                                                                                      test))
                else:
                    Log.objects.get_or_create(import_history=import_history, status='Обновление',
                                              comment='Формула {} ({}) для теста {} уже существует'.format(label,
                                                                                                           formula,
                                                                                                           test))
            else:
                Log.objects.get_or_create(import_history=import_history, status='Ошибка', comment=is_f_valid)
                import_history.import_over = datetime.datetime.now()
                import_history.status = 'Ошибка'
                import_history.save()
                return

    import_history.import_over = datetime.datetime.now()
    import_history.status = 'Успешно'
    import_history.save()


# Функция валидации формулы на операнты (номера вопросов не проверяются)
def is_valid(formula):
    answers = re.findall(r'\w+', formula)

    # Другого способа не придумал
    for operand in answers:
        find = re.search(r'\w\w', operand)

        if operand.isdigit():
            continue
        elif find.group(0) == 'вт':
            continue
        elif find.group(0) == 'ву':
            continue
        elif find.group(0) == 'вр':
            continue
        else:
            return '"{}" - ошибочный операнд! Проверьте формулу: "{}"'.format(find.group(0), formula)

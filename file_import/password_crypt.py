import os
import csv
from Cryptodome.Cipher import AES
from Cryptodome.Hash import SHA256
from Cryptodome import Random

from file_import.models import UsersInfo
from testing_service import settings
from user_profile.models import Institution


def write_file(data):
    headlines = ('{};{};{};{};{};{};{}\n').format('Логин', 'Пароль', 'Учебное заведение', 'Класс', 'Фамилия', 'Имя', 'Отчество')
    create_headlines = False
    csv_reader_data = csv.reader(data.values(), delimiter=';')
    add_in_path_name=""
    for row in csv_reader_data:
        # если название учреждения совпадает
        print(row[2])
        if Institution.objects.get(institution_name=row[2]).institution_name:
            institution = row[2]
            # берем путь к файлу этого учреждения из таблицы
            path = Institution.objects.get(institution_name=row[2]).file_path
            # если такого файла не существует то заголовки должны быть
            if not os.path.exists(path):
                create_headlines = True

            with open(path, 'a') as f:
                # записываем заголовки если они нужны
                if create_headlines:
                    for line in headlines:
                        f.write(line)
                        create_headlines = False
                # записываем данные
                f.write(('{};{};{};{};{};{};{}\n').format(row[0], row[1], row[2], row[3], row[4], row[5], row[6]))

            add_in_path_name = Institution.objects.get(institution_name=row[2]).institution_name
            f.close()

    csv_reader_data = csv.reader(data.values(), delimiter=';')
    for row in csv_reader_data:
        if Institution.objects.get(institution_name=row[2]).institution_name:
            path = Institution.objects.get(institution_name=row[2]).file_path
            encrypt_file(path, add_in_path_name)
            #os.remove(path)


def get_key():
    password = "testing_service"
    hasher = SHA256.new(password.encode('utf-8'))
    return hasher.digest()


def encrypt_file(path, add_in_path_name):
    key = get_key()
    chunk_size = 64*1024
    path_out = os.path.join(settings.BASE_DIR, 'file_import', 'files', add_in_path_name+'_encrypted_users_info.csv')
    output_file = path_out
    file_size = str(os.path.getsize(path)).zfill(16)
    IV = Random.new().read(16)

    encryptor = AES.new(key, AES.MODE_CBC, IV)

    with open(path, 'rb') as in_file:
        with open(output_file, 'ab+') as out_file:
            out_file.write(file_size.encode('utf-8'))
            out_file.write(IV)

            while True:
                chunk = in_file.read(chunk_size)

                if len(chunk) == 0:
                    break
                elif len(chunk) % 16 != 0:
                    chunk += b' ' * (16 - (len(chunk) % 16))

                out_file.write(encryptor.encrypt(chunk))

    file = open(output_file, 'rb')
    ui, created = UsersInfo.objects.get_or_create(file=file.read())
    file.close()


def decrypt_file(path, add_in_path_name):
    key = get_key()
    chunk_size = 16*1024
    path_out = os.path.join(settings.BASE_DIR, 'file_import', 'files', add_in_path_name+'_encrypted_users_info.csv')
    output_file = path_out

    with open(output_file, 'rb') as in_file:
        file_size = int(in_file.read(16))
        IV = in_file.read(16)

        decryptor = AES.new(key, AES.MODE_CBC, IV)

        with open(path, 'ab') as out_file:
            while True:
                chunk = in_file.read(chunk_size)

                if len(chunk) == 0:
                    break

                out_file.write(decryptor.decrypt(chunk))
            out_file.truncate(file_size)

import csv
import datetime
import os
import random

from django.contrib.auth.base_user import BaseUserManager
from django.contrib.auth.hashers import make_password
from django.contrib.auth.models import User, Group

from file_import.password_crypt import write_file
from import_views.models import Log
from testing_service import settings
from user_profile.models import Profile, Institution
from users_relations.models import UsersRelations, RelationType


def password_generate():
    # генерация пароля
    original_password = BaseUserManager().make_random_password(6)
    return original_password


def login_digits_generate():
    # генерация числ для логина
    start = 0
    end = 6
    digits = ""
    while start < end:
        digits += str(random.randrange(0, 9))
        start += 1
    return digits


def import_users(path, import_history):
    # основная функция импорта
    data = {}
    import_history.rows = len(open(path).readlines()) - 1
    import_history.save()
    with open(path) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=';')
        next(csv_reader)
        for row in csv_reader:
            institution_name = row[1]
            institution_addinfo = row[2]
            user_last_name = row[3]
            user_first_name = row[4]
            profile_patronymic = row[5]
            gender = 0 if row[6] == 'male' else 1
            profile_birth_date = datetime.datetime.strptime(row[7], '%d.%m.%Y')
            profile_user_type = row[8]
            profile_class = row[10]

            user_username = "stud" + login_digits_generate()

            while User.objects.filter(username=user_username).exists():
                # пока такой логин уже существует генерируем новый
                user_username = "stud" + login_digits_generate()

            users_group_add(profile_user_type)

            if User.objects.filter(first_name=user_first_name, last_name=user_last_name).exists():
                # если пользователь с таким именем и фамилией уже существует, проверяем и записываемтолько те данные
                # которые в теории могли измениться
                user = User.objects.get(first_name=user_first_name, last_name=user_last_name)
                profile = Profile.objects.get(user_id=user.pk)
                profile.patronymic = profile_patronymic
                profile.birth_date = profile_birth_date
                profile.is_woman = gender
                profile.school_class = profile_class
                profile.save()
                user.first_name = user_first_name
                user.last_name = user_last_name
                user.save()
                Log.objects.get_or_create(import_history=import_history, status='Обновление',
                                          comment='Обновленны данные для {}'.format(profile.name_with_abbreviation()))
                continue

            # заполнение таблицы учебных заведений
            institution, created = Institution.objects.get_or_create(institution_name=institution_name,
                                                                     additional_information=institution_addinfo,
                                                                     file_path=os.path.join(settings.BASE_DIR,
                                                                                            'file_import', 'files',
                                                                                            institution_name + '_users_info.csv'))

            original_password = password_generate()

            user, created = User.objects.get_or_create(password=make_password(original_password),
                                                       username=user_username,
                                                       first_name=user_first_name,
                                                       last_name=user_last_name,
                                                       is_superuser=0,
                                                       is_staff=0)

            user_type = Group.objects.get(name=profile_user_type)
            user.groups.add(user_type.pk)

            # заполнение таблицы профиля
            profile = Profile.objects.get(user_id=user.pk)
            profile.patronymic = profile_patronymic
            profile.birth_date = profile_birth_date
            profile.institution = institution
            profile.is_woman = gender
            profile.school_class = profile_class
            profile.user_type = user_type
            profile.save()

            Log.objects.get_or_create(import_history=import_history, status='Успешно',
                                      comment='Добавлен профайл для пользователя {} {}'.format(user.first_name,
                                                                                               user.last_name))

            data[user.pk] = ('{};{};{};{};{};{};{}\n').format(user_username, original_password, institution_name,
                                                              profile_class, user_last_name, user_first_name,
                                                              profile_patronymic)

        import_users_relations(path)
        write_file(data)

    import_history.import_over = datetime.datetime.now()
    import_history.status = 'Успешно'
    import_history.save()


def import_users_relations(path):
    # отдельный импорт для расставления связей между пользователями
    with open(path) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=';')
        next(csv_reader)
        for row in csv_reader:
            user_last_name = row[3]
            user_first_name = row[4]
            user = User.objects.get(first_name=user_first_name, last_name=user_last_name)
            user_username = user.pk
            relation_type_1 = row[11]
            relation_description_1 = row[12]
            linked_user_1 = row[13]
            linked_user_2 = row[16]
            relation_type_2 = row[14]
            relation_description_2 = row[15]

            if relation_type_1 != "":
                user_relation_type_add(relation_type_1, relation_description_1)

            if relation_type_2 != "":
                user_relation_type_add(relation_type_2, relation_description_2)

            if linked_user_1 != "":
                linked_user_1 = found_connected_user(path, row[13])
                user_relation_add(user_username, linked_user_1, relation_type_1)

            if linked_user_2 != "":
                linked_user_2 = found_connected_user(path, row[16])
                user_relation_add(user_username, linked_user_2, relation_type_2)


def user_relation_type_add(relation_type, information):
    # заполнение таблицы типов отношений
    RelationType.objects.get_or_create(
        relation=relation_type,
        description=information
    )


def users_group_add(user_type):
    # привязывание к пользователю групп
    Group.objects.get_or_create(
        name=user_type
    )


def user_relation_add(user_first_id, user_second_id, relation_type):
    # заполнение таблицы отношений между двумя пользователями
    profile_1 = Profile.objects.get(user_id=user_first_id)
    profile_2 = Profile.objects.get(user_id=user_second_id)
    relation = RelationType.objects.get(relation=relation_type)

    UsersRelations.objects.get_or_create(user_id=profile_1.pk, user_second_id=profile_2.pk,
                                         relation_type_id=relation.pk)


def found_connected_user(path, user_in_relation_id):
    # поиск связанного пользователя ( на случай если порядковый номер в файле не совпадает с порядковым номером в БД )
    with open(path) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=';')
        next(csv_reader)
        for row in csv_reader:
            if row[0] == user_in_relation_id:
                user = User.objects.get(first_name=row[4], last_name=row[3])
                linked_user = user.pk
                return linked_user

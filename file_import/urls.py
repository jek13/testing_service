from django.conf.urls import url

from file_import import views

urlpatterns = [
    url(r'^upload_file/', views.upload),
]

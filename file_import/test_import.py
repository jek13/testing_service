import csv
import datetime

from django.contrib.auth.models import Group

from import_views.models import Log
from test_module.models import AnswerOnQuestion, QuestionDesc, Answer, Test, Question
from users_relations.models import AllowedTypes


def import_tests(file, import_history):
    import_history.rows = len(open(file).readlines()) - 1
    import_history.save()
    with open(file) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=';')
        next(csv_reader)
        for row in csv_reader:
            # Формулировка ответа
            answer_description = row[0]
            # Вес ответа
            answer_weight = row[1]
            # Порядковый номе ответа
            answer_number = row[2]

            # Формулировка вопроса
            question_desc = row[3]
            # Порядковый номер вопроса
            question_number = row[4]
            # Имеет ли вопрос множественный овет
            question_manyanswers = True if row[5].lower() == 'да' else False

            # Имя теста
            test_name = row[6]
            # Формулировка теста
            test_description = row[7]
            # Время на прохождение
            time_to_pass = row[8]
            # Доступен ли для прохождения
            is_active = True if row[9].lower() == 'да' else False

            # Тестируемый тип теста
            allowed_type = row[10]

            # Тело импорта. Объекты создаются в порядке начиная с объекта с наименьшим количеством зависимостей
            # Создаем объект формулировки вопроса, если существует - пропускаем
            answer_on_question, created = AnswerOnQuestion.objects.get_or_create(description=answer_description)

            # Если ячейки с формулировкой вопроса и флагом не пустые - создаём новый объект QuestionDesc
            # и сохраняем его в переменную
            if row[3] and row[5]:
                qd, created = QuestionDesc.objects.get_or_create(question_desc=question_desc,
                                                                 answer_number=question_manyanswers)

            # Если объект вопроса уже существует, но данные изменились - изменяем существующий вопрос,
            # если объекта вопроса не существует - создаём новый

            answer, created = Answer.objects.update_or_create(answer=answer_on_question, answer_question=qd,
                                                              number=answer_number,
                                                              defaults={'answer_weight': answer_weight})
            if not created:
                Log.objects.get_or_create(import_history=import_history, status='Обновление',
                                          comment='Обновлен ответ {}'.format(answer))

            # Если колонка с именем теста не пустая - создаём или сохраняем существующий объект теста, а так же
            # добавляем связь в таблицу AllowedTypes с текущим тестом и значением колонки "Тип тестируемого"
            if row[6]:
                test, created = Test.objects.get_or_create(name=test_name, description=test_description,
                                                           time_to_pass=time_to_pass,
                                                           is_active=is_active)

                if created: Log.objects.get_or_create(import_history=import_history, status='Успешно',
                                                      comment='Добавлен тест {}'.format(test.name))

                group, created = Group.objects.get_or_create(name=allowed_type)
                AllowedTypes.objects.get_or_create(test_id=test.pk, user_type=group)
            # Если ячейка с формулировкой вопроса не пустая, создаем объект вопроса со связью с этой ячейкой
            # (создается тут, потому что сначала должен создаться объект теста)
            if row[3]:
                Question.objects.get_or_create(number=question_number, question_desc_id=qd.pk, test_id=test.pk)

    import_history.import_over = datetime.datetime.now()
    import_history.status = 'Успешно'
    import_history.save()

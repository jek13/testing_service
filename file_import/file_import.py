import os

from file_import.formuls_import import import_formuls
from file_import.import_results import import_results
from file_import.profile_import import import_users
from file_import.test_import import import_tests
from testing_service import settings


def upload_file(file, import_history):
    path = os.path.join(settings.BASE_DIR, 'file_import', 'files', file.name)
    handle_uploaded_file(file, path)
    if file.name == 'tests.csv':
        import_tests(path, import_history)
    elif file.name == 'profiles.csv':
        import_users(path, import_history)
    elif file.name == 'formuls.csv':
        import_formuls(path, import_history)
    else:
        import_results(path, import_history)


def handle_uploaded_file(f, path):
    destination = open(path, 'wb+')
    for chunk in f.chunks():
        destination.write(chunk)
    destination.close()

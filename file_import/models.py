from django.db import models


class UsersInfo(models.Model):
    """Информация о пользователях"""

    class Meta:
        db_table = 'users_info'

    file = models.BinaryField(verbose_name='Данные файла')
    add_date = models.DateTimeField(verbose_name='Дата добавления', auto_now=True)

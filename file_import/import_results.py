import datetime
import sqlite3

from django.contrib.auth.models import User

from import_views.models import Log
from test_module.models import TestPassing, ResultOnQuestion, QuestionDesc, Test, AnswerOnQuestion


def import_results(path, import_history):
    # Открываем скаченную базу данных (далее БД)
    connect = sqlite3.connect(path)
    db = connect.cursor()

    import_history.rows = db.execute('select count(ROWID) from result_on_question').fetchone()[0]
    import_history.save()

    # Береём все результаты
    results = db.execute('select * from result_on_question').fetchall()
    # Берем все прохождения
    tests_passing = db.execute('select * from test_passing').fetchall()

    # Внешний счетчик для цикла с результатами, чтобы каждый раз продолжать с того результата, на котором закончили
    counter = 0
    for test_passing in tests_passing:
        primary_key = test_passing[0]
        date_pass = test_passing[1]
        time_pass = test_passing[2]
        conclusion = test_passing[3]
        test_id = test_passing[4]

        # Проверяем, соответствует ли тест из импортируемой БД нашей БД
        their_test = db.execute('select name from test where ROWID = ?', (test_id,)).fetchone()[0]
        if Test.objects.filter(name=their_test).exists():
            local_test = Test.objects.get(name=their_test)
        else:
            Log.objects.get_or_create(import_history=import_history, status='Ошибка',
                                      comment='Такого теста не существует: {}'.format(their_test))
            import_history.import_over = datetime.datetime.now()
            import_history.status = 'Ошибка'
            import_history.save()
            return

        user_id = test_passing[5]

        # Тоже самое для пользователя
        their_user = db.execute('select username from auth_user where ROWID = ?', (user_id,)).fetchone()[0]
        if User.objects.filter(username=their_user).exists():
            local_user = User.objects.get(username=their_user)
        else:
            Log.objects.get_or_create(import_history=import_history, status='Ошибка',
                                      comment='Такого пользователя не существует: {} '.format(their_user))
            import_history.import_over = datetime.datetime.now()
            import_history.status = 'Ошибка'
            import_history.save()
            return

        again_pass = test_passing[6]
        test_complete = test_passing[7]

        # Проверяем существует ли такой тест в нашей БД, если да - пропускаем его.
        if TestPassing.objects.filter(date_pass=date_pass).exists():
            counter += \
                db.execute('select count(ROWID) from result_on_question where test_id=?',
                           (primary_key,)).fetchone()[0]

            continue

        p_test, created = TestPassing.objects.get_or_create(date_pass=date_pass, time_pass=time_pass,
                                                            conclusion=conclusion,
                                                            test=local_test, user_id=local_user,
                                                            again_pass=again_pass,
                                                            test_complete=test_complete)
        if created:
            Log.objects.get_or_create(import_history=import_history, status='Успешно',
                                      comment='Создан новый процесс прохождения теста: {}'.format(p_test))

        while counter < len(results):
            ball = results[counter][1]

            # Проверяем соответствие формулировки вопроса из импортируемой БД нашей БД
            answer_id = results[counter][2]
            their_answer = \
                db.execute('select description from answer_on_question where ROWID = ?', (answer_id,)).fetchone()[0]
            if AnswerOnQuestion.objects.filter(description=their_answer).exists():
                local_answer = AnswerOnQuestion.objects.get(description=their_answer)
            else:
                Log.objects.get_or_create(import_history=import_history, status='Ошибка',
                                          comment='Такого ответа на вопрос не существует: {}'.format(their_answer))
                import_history.import_over = datetime.datetime.now()
                import_history.status = 'Ошибка'
                import_history.save()
                return

            # Тоже самое для вопроса
            question_id = results[counter][3]
            their_question = db.execute(
                'select question_desc from questions_desc where ROWID = ?', (question_id,)).fetchone()[0]
            if QuestionDesc.objects.filter(question_desc=their_question).exists():
                local_question = QuestionDesc.objects.get(question_desc=their_question)
            else:
                Log.objects.get_or_create(import_history=import_history, status='Ошибка',
                                          comment='Такого вопроса не существует: {}'.format(their_question))
                import_history.import_over = datetime.datetime.now()
                import_history.status = 'Ошибка'
                import_history.save()
                return

            # print('Добавили {} {} {} {}'.format(ball, answer_id, local_question.pk, p_test.pk))

            result, created = ResultOnQuestion.objects.get_or_create(ball=ball, answer=local_answer,
                                                                     question=local_question,
                                                                     test=p_test)
            if created:
                Log.objects.get_or_create(import_history=import_history, status='Успешно',
                                          comment='Добавлен новый ответ на вопрос: {}'.format(result))

            if counter + 1 < len(results):
                counter += 1
            else:
                break
            # Если перескочили на следующий id прохождения теста - останавливаем цикл и
            # проверяем следующее прохождение теста.
            if results[counter - 1][4] != results[counter][4]:
                break

    import_history.import_over = datetime.datetime.now()
    import_history.status = 'Успешно'
    import_history.save()

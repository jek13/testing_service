# -*- coding: utf-8 -*-
from django.db import models
from django.contrib.auth.models import User, Group
from django.db.models.signals import post_save
from django.dispatch import receiver
from util.routing_util import has_group


class Institution(models.Model):
    '''Учреждение'''

    class Meta:
        db_table = 'institutions'
        verbose_name = 'Учреждение'
        verbose_name_plural = 'Учреждения'

    institution_name = models.CharField(verbose_name='Наименование', null=False, blank=True, max_length=256)
    additional_information = models.TextField(verbose_name='Дополнительная информация')
    file_path = models.CharField(verbose_name='Путь до файла', blank=True, null=True, max_length=200)

    def __str__(self):
        return self.institution_name


# Профиль пользователя
class Profile(models.Model):
    class Meta:
        db_table = 'user_profile'
        verbose_name = 'Профиль'
        verbose_name_plural = 'Профили'

    user = models.OneToOneField(User, verbose_name="Пользователь", on_delete=models.CASCADE)
    # Отчество
    patronymic = models.CharField(verbose_name="Отчество", max_length=50, null=True, blank=True)
    # Дата рождения
    birth_date = models.DateField(verbose_name="Дата рождения", default="1111-11-11")
    # Класс, в котором обучается (только для учащихся)
    school_class = models.CharField(verbose_name="Класс (только для учащихся)", max_length=10, null=True, blank=True)
    # Учреждение
    institution = models.ForeignKey(Institution, blank=True, null=True, verbose_name="Учреждение",
                                    on_delete=models.CASCADE)
    # Флаг Ж/М пола
    is_woman = models.BooleanField(verbose_name="Пол (женский)", default=0)
    # Тип пользователя
    user_type = models.ForeignKey(Group, null=True, verbose_name="Тип пользователя",
                                  on_delete=models.CASCADE)

    def name_with_abbreviation(self):
        return '{} {}. {}.'.format("" if not self.user.last_name else self.user.last_name,
                                   "" if not self.user.first_name else self.user.first_name[0],
                                   "" if not self.patronymic else self.patronymic[0])

    def full_name(self):
        return '{} {} {}'.format(self.user.first_name, self.patronymic, self.user.last_name)

    def is_manager(self):
        return has_group(self.user, "Менеджеры")

    def is_teacher(self):
        return has_group(self.user, "Преподаватели")

    def __str__(self):
        return '{} {} {}'.format(self.full_name(), self.birth_date,
                                 "Не учащийся" if not self.school_class else self.school_class)

    @receiver(post_save, sender=User)
    def create_user_profile(sender, instance, created, **kwargs):
        if created:
            Profile.objects.create(user=instance)

    @receiver(post_save, sender=User)
    def save_user_profile(sender, instance, **kwargs):
        instance.profile.save()

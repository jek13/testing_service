from django.conf.urls import url

from user_profile import views

urlpatterns = [
    url(r'^load_users_data/(?P<institution_id>\d+)/$', views.load_user_data)
]

import os
from wsgiref.util import FileWrapper

from django.http import HttpResponse
from django.shortcuts import render, redirect

from user_profile.models import Institution

from file_import.password_crypt import decrypt_file, encrypt_file

from testing_service import settings

#from file_import.latinizator import latinizator


# view скачивания файла с информацией о пользователях
def load_user_data(request, institution_id=1):
    # если есть школа с таким айди
    print(institution_id)
    if Institution.objects.get(id=institution_id).id:
        # берем путь к файлу из БД
        path = Institution.objects.get(id=institution_id).file_path
        # переменная с названием учреждения в дальнейшем для дополнения имени файла
        add_in_path_name = Institution.objects.get(id=institution_id).institution_name
        decrypt_file(path, add_in_path_name)

        # переписываем название школы транслитом для корректного скачивания файла
        # add_in_name = latinizator(Institution.objects.get(id=institution_id).institution_name)
        #print(add_in_name)

        # Пока что добавляем айди учреждения к имени файла
        filename = str(Institution.objects.get(id=institution_id).id) + "_users_info.csv"
        # отправляем файл
        file = open(path, 'rb')
        response = HttpResponse(FileWrapper(file), content_type='application/vnd.ms-excel')
        response['Content-Disposition'] = 'attachment; filename=' + filename
        response['Content-Type'] = 'application/vnd.ms-excel;'
        # удаляем расшифрованный файл
        #os.remove(path)
        return response

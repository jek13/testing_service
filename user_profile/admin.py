from django.contrib import admin
from user_profile.models import *


class ProfileAdmin(admin.ModelAdmin):
    list_filter = ['school_class']


admin.site.register(Profile, ProfileAdmin)
admin.site.register(Institution)


def has_group(user, name):
    '''Проверка наличия группы у пользователя'''

    for group in user.groups.all():
        if group.name == name:
            return True
    return False

